output "vpc-id" {
  value = module.vpc.vpc_id
}

output "sg-id" {
  value = module.web_server_sg.security_group_id
}

output "web-ip" {
  value = module.ec2_instance.public_ip
}