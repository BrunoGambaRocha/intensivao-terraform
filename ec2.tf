module "ec2_instance" {
  source  = "terraform-aws-modules/ec2-instance/aws"

  name = "single-instance"

#   for_each = toset(["one", "two"])
#   name = "instance-$(each.key)"

  ami                    = lookup(var.amis, var.region)
  instance_type          = var.instance_type
  monitoring             = false
  vpc_security_group_ids = [module.web_server_sg.security_group_id]
  subnet_id              = module.vpc.public_subnets[0]

  associate_public_ip_address = true

  user_data_base64       = base64encode(local.user_data)

  tags = {
    Terraform   = "true"
    Environment = var.ambiente
  }

  depends_on = [ module.vpc, module.web_server_sg ]
}

locals {
  user_data = <<-EOT
    #!/bin/bash
    sudo apt update -y &&
    sudo apt install -y nginx
    echo "Hello Nginx - Imersao Terraform com Joao Victor Sell" > /var/www/html/index.html
  EOT
}