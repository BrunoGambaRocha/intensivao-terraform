variable "ambiente" {
  type = string
  default = "Dev"
}

variable "region" {
  description = "Região Padrão"
  default = "us-east-1"
  type = string
}

variable "instance_type" {
  type = string
}

variable "ips" {
  description = "IPs Liberados"
  default = ["152.248.83.10/32","152.248.83.11/32"]
  type = list(string)
}

variable "amis" {
  type = map
  default = {
    "us-east-1" = "ami-0fe0238291c8e3f07"
    "sa-east-1" = "ami-095ca107fb46b81e6"
  }
}