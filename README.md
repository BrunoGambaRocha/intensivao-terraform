# Intensivão Terraform - IaC
- Cloud Treinamentos
- Prof. João Vitor Sell

### Desafio proposto
- Utilizar o Terraform para provisionar uma infra com uma EC2 executando um servidor Nginx

### Pré requisitos
- Conta na AWS
- Criar um usuário programático no IAM na AWS
- Conta na Terraform Cloud
- Configurar projeto Terraform Cloud com as variáveis de ambiente da AWS
- Terraform CLI instalado e logado

##### Clonar o projeto
	git clone git@gitlab.com:BrunoGambaRocha/intensivao-terraform.git
	cd intensivao-terraform

##### Ajustes no projeto
	altere o arquivo terraform.tf com as informações do seu projeto

##### Subindo o projeto
	terraform init
	terraform plan
	terraform apply
	terraform destroy

##### Observações
- Ambiente de Desenvolvimento: ```terraform plan --var-file="inventories/dev.tfvars"```
- Ambiente de Homologação: ```terraform plan --var-file="inventories/hml.tfvars"```
- Ambiente de Produção: ```terraform plan --var-file="inventories/prd.tfvars"```