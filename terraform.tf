terraform {

  cloud {
    hostname     = "app.terraform.io"
    organization = "brunoti"

    workspaces {
      name = "intensivao"
    }
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }

  required_version = ">= 1.1.0"
}